import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';


import Layout from "./components/Layout/Layout";
import Counter from "./containers/Counter/Counter";
import TaskList from "./containers/TodoList/TodoList";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/counter" component={Counter}/>
                    <Route path="/" component={TaskList}/>
                    <Route render={() => <h1>Not found!</h1>}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
