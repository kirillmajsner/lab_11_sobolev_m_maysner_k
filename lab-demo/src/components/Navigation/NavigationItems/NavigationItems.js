import React from 'react';
import './NavigationItems.css';
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => (
    <ul className="NavigationItems">
        <NavigationItem to="/" exact>Todo List</NavigationItem>
        <NavigationItem to="/counter">Counter</NavigationItem>
    </ul>
);

export default NavigationItems;
