import React, {Component} from 'react';

import './AddTask.css';

import {connect} from "react-redux";
import {addTodo, fetchTodo} from "../../store/actions";

class AddTask extends Component {
    state = {
        tasks: ''
    };

    addHandler = () => {
        const task = {text: this.state.tasks};
        this.props.addTodo(task);
    };


    changeHandler = event => {
        const {name, value} = event.target;
        this.setState({[name]: value})
    };

    componentDidMount() {
        this.props.getTasks();
    }


    render() {

        return (
            <div className="TasksForm">
                <input type="text" name="tasks" onChange={this.changeHandler} value={this.state.tasks}
                       placeholder="Add new task"/>
                <button onClick={this.addHandler} className="addTodo">add</button>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        tasks: state.tasks
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addTodo: (task) => dispatch(addTodo(task)),
        getTasks: () => dispatch(fetchTodo())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTask);