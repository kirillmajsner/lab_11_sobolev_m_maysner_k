import React, {Component, Fragment} from 'react';
import './TodoList.css';


import {deleteTask, fetchTodo} from "../../store/actions";
import AddTask from '../../components/AddTask/AddTask';
import {connect} from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";

class TodoList extends Component {


    componentDidMount() {
        this.props.getTasks();
    };

    render() {
        let tasks = [];
        for (let key in this.props.tasks) {
            tasks.push(<div className="TaskList" key={key}>
                <p className="text">{this.props.tasks[key].text}</p>
                <button className="del-btn" onClick={() => this.props.deleteTask(key)}>X</button>
            </div>);
        }
        return (
            <Fragment>
                <AddTask/>
                {this.props.loading ? <Spinner/> : tasks}
            </Fragment>

        );
    }
}

const mapStateToProps = state => {
    return {
        tasks: state.tasks,
        loading: state.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getTasks: () => dispatch(fetchTodo()),
        deleteTask: (taskId) => dispatch(deleteTask(taskId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
