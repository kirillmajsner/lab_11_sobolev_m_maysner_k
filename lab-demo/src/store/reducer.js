import {
    ADD,
    ADD_TODO,
    FETCH_TODO_SUCCESS,
    INCREMENT, REMOVE_TODO,
    DECREMENT,
    SUBTRACT, FETCH_TODO_REQUEST, FETCH_TODO_ERROR, FETCH_COUNTER_REQUEST, FETCH_COUNTER_SUCCESS, FETCH_COUNTER__ERROR
} from "./actions";

const initialState = {
    counter: 0,
    tasks: [],
    loading: false,
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD:
            return {
                ...state,
                counter: state.counter + action.amount,
            };
        case INCREMENT:
            return {
                ...state,
                counter: state.counter + 1,
            };

        case DECREMENT:
            return {
                ...state,
                counter: state.counter - 1,
            };
        case SUBTRACT:
            return {
                ...state,
                counter: state.counter - action.amount,
            };
        case FETCH_COUNTER_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_COUNTER_SUCCESS:
            return {
                ...state,
                counter: action.counter,
                loading: false,
            };
        case FETCH_COUNTER__ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case ADD_TODO:
            return {
                ...state,
            };
        case REMOVE_TODO:
            return {
                ...state

            };
        case FETCH_TODO_SUCCESS:
            return {
                ...state,
                tasks: action.text,
                loading: false
            };
        case FETCH_TODO_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_TODO_ERROR:
            return {
                ...state,
                error: action.error

            };


        default:
            return state
    }
};

export default reducer;
