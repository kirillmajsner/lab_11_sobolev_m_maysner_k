import axios from "../axios-requests";

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';


export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';


export const FETCH_TODO_REQUEST = 'TODO_REQUEST';
export const FETCH_TODO_SUCCESS = 'TODO_SUCCESS';
export const FETCH_TODO_ERROR = 'TODO_ERROR';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER__ERROR = 'FETCH_COUNTER_ERROR';


export const fetchTodoRequest = () => {
    return {type: FETCH_TODO_REQUEST};
};
export const fetchTodoSuccess = text => {
    return {type: FETCH_TODO_SUCCESS, text: text};
};
export const fetchTodoError = error => {
    return {type: FETCH_TODO_ERROR, error};
};


export const incrementCounter = () => {
    return dispatch => {
        dispatch({type: INCREMENT});
        dispatch(saveCounter());
    }
};
export const decrementCounter = () => {
    return dispatch => {
        dispatch({type: DECREMENT});
        dispatch(saveCounter());
    }
};

export const addCounter = amount => {
    return dispatch => {
        dispatch({type: ADD, amount});
        dispatch(saveCounter());
    }
};

export const subtractCounter = amount => {
    return dispatch => {
        dispatch({type: SUBTRACT, amount});
        dispatch(saveCounter());
    }
};


export const fetchCounterRequest = () => {
    return {type: FETCH_COUNTER_REQUEST};
};
export const fetchCounterSuccess = counter => {
    return {type: FETCH_COUNTER_SUCCESS, counter};
};
export const fetchCounterError = error => {
    return {type: FETCH_COUNTER__ERROR, error};
};


export const fetchCounter = () => {
    return dispatch => {
        dispatch(fetchCounterRequest());
        axios.get('/counter.json').then(response => {
            dispatch(fetchCounterSuccess(response.data));
        }, error => {
            dispatch(fetchCounterError())
        });
    }
};

export const saveCounter = () => {
    return (dispatch, getState) => {
        const counter = getState().counter;
        axios.put('/counter.json', counter).then()
    }
};

export const fetchTodo = () => {
    return (dispatch, getState) => {
        dispatch(fetchTodoRequest());
        axios.get('https://lab-todo.firebaseio.com/todo.json').then(response => {
            dispatch(fetchTodoSuccess(response.data));
        }, error => {
            dispatch(fetchTodoError());
        });
    }
};

export const addTodo = (task) => {
    console.log(task);
    return (dispatch) => {
        dispatch(fetchTodoRequest());
        axios.post('/todo.json', task).then(() => {
            dispatch(fetchTodo());
        })
    }
};


export const deleteTask = (id) => {
    console.log(id);
    return (dispatch) => {
        dispatch(fetchTodoRequest());
        axios.delete('/todo/' + id + '.json').then(() => {
            dispatch(fetchTodo());
        })
    }
};