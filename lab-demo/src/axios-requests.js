import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://lab-todo.firebaseio.com/'
});


export default instance;
